﻿using UnityEngine;
using UnityEngine.UI;

public class LightStandManager : MonoBehaviour
{
    [SerializeField] Sprite _LightOn = default;
    [SerializeField] Sprite _LightOff = default;
    [SerializeField] Image _Image = default;
    [SerializeField] GameObject _LightText = default;

    private void Start()
    {
        _LightText.SetActive(false);
    }
    public void SwitchLight(bool isOn)
    {
        _Image.sprite = isOn ? _LightOn : _LightOff;
        _LightText.SetActive(isOn);
    }
}
