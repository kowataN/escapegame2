﻿using UnityEngine;

public class DrawerManager : MonoBehaviour
{
    [SerializeField] GameObject _LightBulb = default;
    [SerializeField] GameObject _AlertText = default;
    [SerializeField] ItemBoxManager _ItemBoxManager = default;

    private void SetActiveLightBulb(bool value)
    {
        _LightBulb.SetActive(value);
        _AlertText.SetActive(value);

    }
    private void Start()
    {
        SetActiveLightBulb(false);

    }

    public void OnClickTrigger()
    {
        SetActiveLightBulb(true);
        _ItemBoxManager.SetItem(ItemType.kLightBlub);

    }

    public void OnClickImage()
    {
        SetActiveLightBulb(false);

    }
}
