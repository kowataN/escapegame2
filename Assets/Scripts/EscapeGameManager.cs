﻿using UnityEngine;

public class EscapeGameManager : MonoBehaviour
{
    public enum PanelType { kRoom, kLight, kPC, kDrawer }

    [SerializeField] GameObject _LightPanel = default;
    [SerializeField] GameObject _PCPanel = default;
    [SerializeField] GameObject _DrawerPanel = default;

    public PanelType _CrtPanel = PanelType.kRoom;

    public void OnClickLight()
    {
        _LightPanel.SetActive(true);
        _CrtPanel = PanelType.kLight;
    }

    public void OnClickPC()
    {
        _PCPanel.SetActive(true);
        _CrtPanel = PanelType.kPC;

    }

    public void OnClickDrawer()
    {
        _DrawerPanel.SetActive(true);
        _CrtPanel = PanelType.kDrawer;

    }

    public void OnClickBack()
    {
        _LightPanel.SetActive(false);
        _PCPanel.SetActive(false);
        _DrawerPanel.SetActive(false);
        _CrtPanel = PanelType.kRoom;


    }
}
