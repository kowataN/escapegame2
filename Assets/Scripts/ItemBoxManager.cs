﻿using UnityEngine;
using UnityEngine.UI;

public enum ItemType
{
    kNone,
    kLightBlub,
}

public class ItemBoxManager : MonoBehaviour
{
    [SerializeField] Sprite _LightBulbSprite = default;
    [SerializeField] Image[] _ItemBoxImages = default;
    [SerializeField] LightStandManager _LightStandManager = default;
    [SerializeField] EscapeGameManager _GameManager = default;

    // 取得したアイテム
    ItemType[] _ItemList = new ItemType[4];

    public void SetItem(ItemType item)
    {
        _ItemList[0] = item;
        switch (item)
        {
            case ItemType.kLightBlub:
                _ItemBoxImages[0].sprite = _LightBulbSprite;
                break;
            default:
                _ItemBoxImages[0].sprite = null;
                break;

        }
    }
    public void UseItem(int index)
    {
        if (_GameManager._CrtPanel == EscapeGameManager.PanelType.kLight
            && _ItemList[index] == ItemType.kLightBlub)
        {
            _LightStandManager.SwitchLight(true);
            _ItemList[index] = ItemType.kNone;
            _ItemBoxImages[index].sprite = null;

        }
    }
}
